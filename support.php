<?php
	$title = 'Goods Management Under Fiscal and/or Customs Deposit Regime';
	$page ="goods-managements";
   include "header.php";
?>


<section class="banner services">
	<div class="container">
		<div class="col-md-12">
			<div class="banner-content">
				<h1 class="banner-content-title">Goods Management Under Fiscal and/or Customs Deposit Regime</h1>
				<!-- <p class="banner-content-paragraph">Reliable Tank Storage for all your needs</p> -->
			</div>
	   </div>
	</div>
</section>
<!-- ========================================================================================================= -->
                 <!-- HISTORY OF SOVREDMET -->
<!-- ========================================================================================================= -->
<section class="services-service section">
	<div class="container">
		<div class="row">
			 <?php
					// $title = 'Services - Blending';
					// $page ="blending";
				   include "service-menu.php";
				?>
			<div class="col-md-8">
				<div class="blending-img">
					<img src="img/services/services/dispatching2.jpg" class="img-fluid" alt="">
				</div>
			</div>
		</div>
	</div>
</section>

<!-- ========================================================================================================= -->
                 <!-- blending content -->
<!-- ========================================================================================================= -->
<section class="blending-content section">
	<div class="container">
	       <div class="row" style="margin-bottom: 60px">
            	<div class="col-md-6">
            		 <div class="features" style="padding-top: 0">
		            	<h5>SOVREDMET TRUCK FLEET</h5>         
						 <p>              
							With access to a network of fleet partners, we’re positioned to deliver refined products and fuels anywhere in the nation. All petroleum distribution companies deliver their gasoline and diesel fuel products via truck transportation. Petroleum trucks are categorized by two basic sizes: transport and tank wagon. The word transport and tank wagon describe the general capacity of the size of the “tanker” that the truck is hauling. Some petroleum distribution truck fleets rely heavily on transport tankers, while other petroleum companies mostly maintain tank wagon-sized tankers. Our fleet partners own and operate a delivery fleet that maintains both transport and tank wagon trucks. The versatility of delivery is a key component to successfully serving our customers’ wide variety of needs. 
			              </p>
					
		            </div>
            	</div>

            	<div class="col-md-6">
            		 <div class="features" style="padding-top: 0">
            		<img src="img/services/services/truck2.jpg" class="img-fluid" alt="">
            		 </div>
            	</div>
            </div>

              <div class="row" style="margin-bottom: 60px">
            	

            	<div class="col-md-6">
            		 <div class="features" style="padding-top: 0">
            		<img src="img/services/services/dispatching.jpg" class="img-fluid" alt="">
            		 </div>
            	</div>

            	<div class="col-md-6">
            		 <div class="features" style="padding-top: 0">
		            	<h5>SOVREDMET SHIP FLEET</h5>

						 <p>
						 	We have the equipment and expertise to help you move mission-critical materials with worry-free consistency across your supply chain and around the world.
							We create value for our Customers by moving and managing a wide variety of materials in both inland and offshore marine markets—all at the highest safety standards. Our experience transporting, loading and unloading, and managing vessels of any size means your cargo arrives where you need it, when you need it—safely, and with complete visibility
			              </p>
					
		            </div>
            	</div>
            </div>

              <div class="row" >
            	<div class="col-md-6">
            		 <div class="features" style="padding-top: 0">
		            	<h5>SOVREDMET RAILING</h5>
						 <p>
							sovredmet has played an increasingly important role in the movement of crude oil. sovredmet infrastructure is vital to moving crude oil produced in new fields to markets. Our loading facilities for crude oil have been or are being constructed in virtually every new production area of the Russia. In sovredmet, Petroleum is transported via our leading private railroad transportation operator. We operate more than 18 000 units of rolling-stock. Car fleet is high diversified and has 12 types of units, including universal and specialized carriages. On these qualifications, sovredmet provides transportation services for wide range of industrial cargoes and we offer the following services: Transportation in own rolling-stock of the company, Forwarding services for clients, Scheduling and rail cars management, Tracing and dispatcher's control, Improving technological procedures of loading and unloading, Integrated services to consignors and consignees, Crude By Rail Safety Course, etc.
			              </p>
					
		            </div>
            	</div>

            	<div class="col-md-6">
            		 <div class="features" style="padding-top: 0">
            		<img src="img/services/services/s7.jpg" class="img-fluid" alt="">
            		 </div>
            	</div>
            </div>
	</div>	
</section>
<section class="non-home-contact-btn">
	<div class="container">
		<div class="col-ms-12">
			<div class="containing">
				<h5 class="">Contact Us For Your Product Transportation Now!</h1>
		    		<a href="contact.php" class="btn btn-primary animated-button">
		    	  <span></span>
				  <span></span>
				  <span></span>
				  <span></span>
		    	Contact Us
		    </a>
			</div>
		</div>
		
	</div>
</section>



<!--=========================footer =============================================-->
 <!-- ----------footer---------------------------------- -->
 <?php
	$page ="goods-managements";
	include "footer.php";
?>
<!-- ---------------end footer ---------------------------- -->
