
<?php
	$title = 'IV GRAPHICS';
	$page ="iv-graphices";
	include "includes/header.php";
?>

<section id="iv-graphics-bg">
	<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 left">
           <div class="contain-top">
              <img src="img/iv-graphics/icons/IV Graphics-01.png" alt="iv-render sub-logo">
		   </div>
		   <div class="contain-down">
            <div class="content-wrap">
				<h2 class="top"><span>WE</span> DEVELOP</h2>
				<h2 class="down">DESIGNS WITH MEANING</h2>
				<p>We are a branding, printing, and graphic design studio
					based in Lagos, Nigeria</p>
			
            </div>
                   
           </div>
        </div>
        <div class="col-md-6 right">
			<div class="contain-top"><h3><a class="nav-link <?php if($page=='iv-graphics'){echo 'active';}?>" href="iv-graphics-work.php">Work</a></h3></div>
          <div class="contain-down">
            <div class="content-wrap">
				<!-- <h2>EXPLORE OUR WORKS</h2> -->
				<div id="iv-graphics"></div>
            </div>
                   
           </div>
        </div>
    </div>
	</div>
</section>
<!-- ========================================================================================================= -->
                 <!-- CATEGORY -->
<!-- ========================================================================================================= -->
<section class="section-our-expertise">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="content">
					       <h3>Our Expertise</h3>
						   <p>We develop contemporary brand identities, logo design, printing solutions and digital experiences 
							   for broad range of businesses, ranging from enterprises to global businesses.</p>
					   </div>
				</div>
			</div>
		</div>
</section>


<section class="section-a">
	<section class="section-main">
		<div class="container">
			<div class="row">
				<div class="col-md-6 heading">
					<div class="content">
                       <div class="content-wrap"> 	 
					   <h3>Our Expertise</h3>  
					   </div>
					</div>
				</div>
				<div class="col-md-6 text">
					<div class="content">
                       <div class="content-wrap"> 
						   <p>We develop contemporary brand identities, logo design, printing solutions and digital experiences 
							   for broad range of businesses, ranging from enterprises to global businesses.</p>
					   </div>
					</div>
				</div>
			</div>
		</div>
      
	</section>
	<section class="img">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
			  	<img src="img/iv-graphics/SYNTEXA.jpg" class="img-fluid" alt="img-fluid">
				</div>
			</div>
		</div>
	</section>
   
</section>


<section class="section-a">
	<section class="section-main">
		<div class="container">
			<div class="row">
			<div class="col-md-6 text">
					<div class="content">
                       <div class="content-wrap"> 
						   <p>Branding is high necessary when trying to generate future business. Strongly established brand 
							   can increase a business’ value by giving the company more leverage in the industry. 
							   This makes it a more appealing investment opportunity because of its firmly 
							   established place in the marketplace.
							</p>
					   </div>
					</div>
				</div>
				<div class="col-md-6 heading">
					<div class="content">
                       <div class="content-wrap"> 	 
					   <h3>Brand for industries</h3>  
					   </div>
					</div>
				</div>
				
			</div>
		</div>
      
	</section>
	<section class="img">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
			  	<img src="img/iv-graphics/SOAN.jpg" class="img-fluid" alt="img-fluid">
				</div>
			</div>
		</div>
	</section>
   
</section>

<section class="section-a">
	<section class="section-main">
		<div class="container">
			<div class="row">
				<div class="col-md-6 heading">
					<div class="content">
                       <div class="content-wrap"> 	 
					   <h3>Brand for fashion</h3>  
					   </div>
					</div>
				</div>
				<div class="col-md-6 text">
					<div class="content">
                       <div class="content-wrap"> 
						   <p>Brand strategies and management rules can be applied in any industry and type of brand. But for the fashion sector it has been a more typical approach because fashion brands rely more in their public identity. We carefully take measures when developing identities and strategies and always keep up with trends so as to meet client’s vision.</p>
					   </div>
					</div>
				</div>
			</div>
		</div>
      
	</section>
	<section class="img">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
			  	  <div class="svg-wrap">
			  	  	<div id="brand-for-fashion-svg"></div>
			  	  </div>
				</div>
			</div>
		</div>
	</section>
   
</section>


<section class="section-a">
	<section class="section-main">
		<div class="container">
			<div class="row">
			<div class="col-md-6 text">
					<div class="content">
                       <div class="content-wrap"> 
						   <p>
								Consumers are constantly going for products that will help them enhance, repair or maintain the features of their skin. With several research from the cosmetic industry,
								we often develop brand strategy that take range from simplistic, luxury to ornate. Growing brands and increase awareness is our pride.
							</p>
					   </div>
					</div>
				</div>
				<div class="col-md-6 heading">
					<div class="content">
                       <div class="content-wrap"> 	 
					   <h3>Brand for skin</h3>  
					   </div>
					</div>
				</div>
				
			</div>
		</div>
      
	</section>
	<section class="img">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
			  	<img src="img/iv-graphics/SAKE-NATURAL.jpg" class="img-fluid" alt="img-fluid">
				</div>
			</div>
		</div>
	</section>
   
</section>

<section class="section-a">
	<section class="section-main">
		<div class="container">
			<div class="row">
				<div class="col-md-6 heading">
					<div class="content">
                       <div class="content-wrap"> 	 
					   <h3>Brand for art</h3>  
					   </div>
					</div>
				</div>
				<div class="col-md-6 text">
					<div class="content">
                       <div class="content-wrap"> 
						   <p>Art not only tells an important verifiable story, but it can moreover act as an ethical compass in present day times, challenging recognitions, empowering sympathy, and advancing activity. We don’t just partner with brands that run the Arts world to promote interesting art. We must as well be interesting ourselves atheistically.</p>
					   </div>
					</div>
				</div>
			</div>
		</div>
      
	</section>
	<section class="img">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
			  	<img src="img/iv-graphics/CLASSICAL.jpg" class="img-fluid" alt="img-fluid">
				</div>
			</div>
		</div>
	</section>
   
</section>

 <?php
    $class ="iv-graphics";
    $text="Building Your Brand and Increase Sales is our Pride";
    include "includes/call-to-action.php";
 ?> 

<!--=========================footer =============================================-->
 <!-- ----------footer---------------------------------- -->
 <?php
	$page ="iv-graphices";
    include "includes/footer.php";
?>
<!-- ---------------end footer ---------------------------- -->
