
<?php
	$title = 'IV RENDER';
	$page ="iv-render";
	include "includes/header.php";
?>

<section id="iv-render">
	<div class="iv-render banner">
	<div class="container iv-container">
		<div class="row iv-row">
				<div class="col-md-12 iv-sub">
					<div class="sub-menu">
						<div class="logo">
							<img src="img/iv-render/icons/sub-logo.png" alt="iv-render sub-logo">
							<span><a class="nav-link <?php if($page=='iv-render'){echo 'active';}?>" href="expLore-our-projects.php">explore our work</a></span>
						</div>
					</div>
			</div>
			<div class="col-md-12 iv-content">
					<div class="main-content">
						<div class="content-wrap">
							<h3>3D VISUALIZATION</h3>
							<h2>WITH MODERNITY</h2>
						</div>
					</div>
			</div>
		</div>
	</div>
	</div>
</section>
<!-- ========================================================================================================= -->
                 <!-- CATEGORY -->
<!-- ========================================================================================================= -->
<section class="iv-render category">
   <div class="container-fluid">
	<div class="row">
	  <div class="col-md-4 img-4">
	    <div class="content">
		   <img src="img/iv-render/500 x 800.jpg" class="img-fluid" alt="Office Space copy_cut image">
		   <div class="img-content">
		    <h4>interior rendering</h4>
			<p>Natural plus modern</p>
		   </div>
		</div>
	  </div>
	  <div class="col-md-8 img-8">
		<div class="row">
		   <div class="col-md-12 img-12 img1">
		   <div class="content">
		       <div class="img-content">
			      <h4>exterior rendering</h4>
					<p>Creating places that enhance the human experience</p>
				</div>
				<img src="img/iv-render/1000 x 400.jpg" class="img-fluid" alt="Office Space copy image">
			</div>
		   </div>
		   <div class="col-md-12 img-12 img2">
		   <div class="content">
				<img src="img/iv-render/1000 x 400 (2).jpg" class="img-fluid" alt="Simple_Interior2 image">
				<div class="img-content">
			        <h4>architectural animation</h4>
					<p>Experience your project with CG animations</p>
				</div>
			</div>
		   </div>
		</div>
	  </div>
	</div>
   </div>
</section>

<!-- ========================================================================================================= -->
                 <!-- CAPABILITY -->
<!-- ========================================================================================================= -->
<section class="iv-render capability">
   <div class="container">
	<div class="row">
		<div class="col-md-12 ">
			<div class="heading">
				<h2>our capabilities</h2>
				<div class="underscore"></div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="content">
			    <div class="img">
					<img src="img/iv-render/icons/3d Renderinf Icon-01.png" alt="3d Animation-01">
				</div>
				<div class="main-content">
					<h4>3D rendering</h4>
					<p>Present your projects to your prospects by using high quality photorealistic
				 and stunning rendering that increases the chance of getting sales.</p>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="content">
			    <div class="img">
					<img src="img/iv-render/icons/Interior Design-01.png" alt="Interior Design-01">
				</div>
				<div class="main-content">
					<h4>INTERIOR DESIGN</h4>
					<p>Let us bring your dream home to life. As 3D experts, we strive to 
						create better interior spaces for the best looks and functions.</p>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="content">
			    <div class="img">
					<img src="img/iv-render/icons/3d Animation-01.png" alt="3d Animation-01">
				</div>
				<div class="main-content">
					<h4>3D ANIMATION</h4>
					<p>Give a detailed introduction to your architectural project as it will enable
						 clients/prospects to visualize the design, interiors
						  and special qualities of the project in a better way.</p>
				</div>
			</div>
		</div>
	 
	</div>
   </div>
</section>

<!-- ========================================================================================================= -->
                 <!-- INTUITIVE PROCESS-->
<!-- ========================================================================================================= -->
<section class="iv-render intuitive">
   <div class="container">
	<div class="row">
		<div class="col-md-12 ">
			<div class="heading">
				<h2>our intuitive process</h2>
				<div class="underscore"></div>
			</div>
		</div>
		<div class="col-md-5 slider">
			<div class="content">
			<div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
				<div class="carousel-inner">
					<div class="carousel-item active">
						<img src="img/iv-render/slides/1.jpg" class="d-block w-100" alt="...">
					</div>
					<div class="carousel-item">
					  <img src="img/iv-render/slides/2.jpg" class="d-block w-100" alt="...">
					</div>
					<div class="carousel-item">
					   <img src="img/iv-render/slides/3.jpg" class="d-block w-100" alt="...">
					</div>
					<div class="carousel-item ">
						<img src="img/iv-render/slides/4.jpg" class="d-block w-100" alt="...">
					</div>
					<div class="carousel-item">
					  <img src="img/iv-render/slides/5.jpg" class="d-block w-100" alt="...">
					</div>
					<div class="carousel-item">
					   <img src="img/iv-render/slides/6.jpg" class="d-block w-100" alt="...">
					</div>
					<div class="carousel-item">
						<img src="img/iv-render/slides/7.jpg" class="d-block w-100" alt="...">
					</div>
					<div class="carousel-item">
					  <img src="img/iv-render/slides/8.jpg" class="d-block w-100" alt="...">
					</div>
					<div class="carousel-item">
					   <img src="img/iv-render/slides/9.jpg" class="d-block w-100" alt="...">
					</div>
					<div class="carousel-item">
					   <img src="img/iv-render/slides/10.jpg" class="d-block w-100" alt="...">
					</div>
					<div class="carousel-item">
					  <img src="img/iv-render/slides/11.jpg" class="d-block w-100" alt="...">
					</div>
					<div class="carousel-item">
					   <img src="img/iv-render/slides/12.jpg" class="d-block w-100" alt="...">
					</div>
					<div class="carousel-item">
					   <img src="img/iv-render/slides/13.jpg" class="d-block w-100" alt="...">
					</div>
				  </div>
				</div>

			</div>
		</div>
		<div class="col-md-7 tabs">
			<div class="tabs">
				<div class="tab-2">
					<label for="tab2-1">24 HOURS START</label>
					<input id="tab2-1" name="tabs-two" type="radio" checked="checked">
					<div class="row">
						<div class="col-md-12">
							<p>Once we receive the brief, we’ll begin with the project as soon as possible.</p>
							<div class="row">
								<div class="col-md-6">
									<div class="tab-content">
										<div class="icon">
											<img src="img/iv-render/icons/Instant Quote-01.png" alt="">
										</div>
										<div class="message">
											<h3>INSTANT QUOTE</h3>
											<p>You don’t need to wait till 24hrs, we can issue a quotation immediately to start with the project.</p>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="tab-content">
										<div class="icon">
											<img src="img/iv-render/icons/Realiable Team-01.png" alt="">
										</div>
										<div class="message">
											<h3>RELIABLE EXPERTS</h3>
											<p>Our readily team of experts are set for your project once the estimate is approved.</p>
										</div>
									</div>
								</div>
							</div>
						
						</div>
					  </div>
				</div>
				<div class="tab-2">
					<label for="tab2-2">3-DAYS SUBMITION</label>
					<input id="tab2-2" name="tabs-two" type="radio">
					<div class="row">
						<div class="col-md-12">
							<p>You are getting your first draft render in just 3 working days.</p>
							<div class="row">
								<div class="col-md-6">
									<div class="tab-content">
										<div class="icon">
											<img src="img/iv-render/icons/Cloud Collaboration-01.png" alt="">
										</div>
										<div class="message">
											<h3>cloud collaboration</h3>
											<p>Our cloud communication tools enable work and submission faster with great feedback to improve workflow process.</p>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="tab-content">
										<div class="icon">
											<img src="img/iv-render/icons/Timing-01.png" alt="">
										</div>
										<div class="message">
											<h3>timing</h3>
											<p>We are always time conscious. If we don’t meet the agreed deadline then you will get your project rendered for free</p>
										</div>
									</div>
								</div>
							</div>
						
						</div>
					  </div>
				</div>

				<div class="tab-2">
					<label for="tab2-3">SMART PROCESS</label>
					<input id="tab2-3" name="tabs-two" type="radio">
					
					<div class="row">
						<div class="col-md-12">
							<p>Our simple and intuitive process will boost rendering in quicker timing.</p>
							<div class="row">
								<div class="col-md-6">
									<div class="tab-content">
										<div class="icon">
											<img src="img/iv-render/icons/Project Manager-01.png" alt="">
										</div>
										<div class="message">
											<h3>PROJECT MANAGER</h3>
											<p>Your 3D Rendering project will be supervised by your Project Manager from beginning to delivering</p>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="tab-content">
										<div class="icon">
											<img src="img/iv-render/icons/CRM-01.png" alt="">
										</div>
										<div class="message">
											<h3>CRM</h3>
											<p>Our CRM system allows us to stay connected with you, streamline processes, and improve profitability.</p>
										</div>
									</div>
								</div>
							</div>
						
						</div>
					  </div>

				</div>
			</div>

		</div>
	 
	</div>
   </div>
</section>

<!--=========================footer =============================================-->
 <!-- ----------footer---------------------------------- -->
 <?php
	$page ="iv-render";
    include "includes/footer.php";
?>
<!-- ---------------end footer ---------------------------- -->
