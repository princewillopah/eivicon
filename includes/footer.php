
<!--=========================footer =============================================-->
    
<footer id="footer">
                    <div class="container">
                            <div class="row">
                               <div class="col-md-9 main">
                                 <div class="row inner">
                                     <div class="col-md-3 inner">
                                      <div class="content">
                                        <div class="wrap">
                                            <h3>PRODUCT</h3>
                                            <nav class="nav flex-column">
                                                  <a class="nav-link" href="#"> IV Graphics</a>
                                                  <a class="nav-link  <?php if($page=='iv-render'){echo 'active';}?>" href="iv-render.php"> IV Render</a>
                                                  <a class="nav-link <?php if($page=='iv-motion'){echo 'active';}?>" href="iv-motion.php"> IV Motion</a>
                                                  <a class="nav-link" href="#"> IV Trend</a>
                                                  <a class="nav-link" href="#"> IV Exclusive </a>
                                          </nav>
                                        </div>
                                      </div>
                                     </div>
                                     <div class="col-md-3 inner">
                                      <div class="content">
                                         <div class="wrap">
                                            <h3>SUPPORT</h3>
                                            <nav class="nav flex-column">
                                                  <a class="nav-link" href="#">Help Center</a>
                                                  <a class="nav-link" href="#">Contact Us</a>
                                              
                                          </nav>
                                         </div>
                                      </div>
                                     </div>
                                     <div class="col-md-3 inner">
                                      <div class="content">
                                         <div class="wrap">
                                            <h3>INSIGHT</h3>
                                            <nav class="nav flex-column">
                                                  <a class="nav-link" href="#">The Team</a>
                                                  <a class="nav-link" href="#">Why Choose ievicon</a>
                                              
                                          </nav>
                                         </div>
                                      </div>
                                     </div>
                                     <div class="col-md-3 inner">
                                      <div class="content">
                                         <div class="wrap">
                                            <h3>CUSTOMERS</h3>
                                            <nav class="nav flex-column">
                                                <a class="nav-link" href="#">Testimonials</a>
                                                <a class="nav-link" href="#">Customer Success</a>
                                                
                                              
                                            </nav>
                                         </div>
                                      </div>
                                     </div>
                                 </div>
                               </div>
                               <div class="col-md-3 side">
                                 <div class="side-box"></div>
                                     <div class="content">
                                      <div class="wrap">
                                              <h3>COMPANY</h3>
                                              <nav class="nav flex-column">
                                                  <a class="nav-link" href="#">About Us</a>
                                                  <a class="nav-link" href="#">Career</a>
                                                  <a class="nav-link" href="#">Privacy & Terms</a>
                                              </nav>
                                              <div class="connect">
                                                <h3>connect with us</h3>
                                                <nav class="nav flex-row">
                                                  <a class="nav-link" href="#"><i class="fa fa-facebook"></i></a>
                                                  <a class="nav-link" href="#"><i class="fa fa-instagram"></i></a>
                                                  <a class="nav-link" href="#"><i class="fa fa-linkedin"></i></a>
                                              </nav>
                                              </div>
                                          </div>
                                      </div>
                               </div>
                            </div><!-- /.row -->
                    </div><!-- /.container -->
                    
                     <div class="copyright-color" >
                            <div class="container">
                                <div class="row ">
                                    <div class="col-12">
                                        <div class="copyright">
                                            © IEVICON 2020
                                        </div><!-- /.copyright -->
                                    </div><!-- /.col-12 -->
                                </div><!-- /.row -->
                            </div><!-- /.container container-fluid -->
                     </div><!-- /.copyright-color -->
         </footer>
  


<!--cdn-->
<!-- <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script> -->
<!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script> -->
<!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js" integrity="sha256-z6FznuNG1jo9PP3/jBjL6P3tvLMtSwiVAowZPOgo56U=" crossorigin="anonymous"></script> -->


<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/aos.js"></script>
<script src="js/main.js"></script>
<script src="js/main2.js"></script>
<script src="js/wow.js"></script>



<script src="js/svgs/home/slide1_lottie.js"></script>
<script src="js/svgs/home/slide2_lottie.js"></script>
<script src="js/svgs/home/slide3_lottie.js"></script>
<script src="js/svgs/home/slide4_lottie.js"></script>
<script src="js/svgs/home/hero_lottie.js"></script>
<script src="js/svgs/home/graph_lottie.js"></script>

<script src="js/svgs/exclusive/pivotcoins_lottie.js"></script>
<script src="js/svgs/exclusive/hoyer_lottie.js"></script>
<script src="js/svgs/exclusive/ekstralayn_lottie.js"></script>
<script src="js/svgs/exclusive/snd_lottie.js"></script>
<script src="js/svgs/exclusive/benco_lottie.js"></script>
<script src="js/svgs/exclusive/deeperlife_lottie.js"></script>
<script src="js/svgs/exclusive/hero_lottie.js"></script>

<script src="js/svgs/graphics/hero_lottie.js"></script>
<script src="js/svgs/graphics/brand-for-fashion-svg-lottie.js"></script>
<script src="js/svgs/graphics/works/first-section-lottie.js"></script>
<script src="js/svgs/graphics/works/fourth-section-a-lottie.js"></script>
<script src="js/svgs/graphics/works/fourth-section-b-lottie.js"></script>
<script src="js/svgs/graphics/works/fifth-lottie.js"></script>
<script src="js/svgs/graphics/works/sixth-lottie.js"></script>
<script src="js/svgs/graphics/works/eight-lottie.js"></script>

<script src="js/svgs/home/homepage-code.js"></script>
<script src="js/svgs/exclusive/iv-exclusive-code.js"></script>
<script src="js/svgs/graphics/iv-graphics-code.js"></script>



<script>
// new WOW().init();
AOS.init({
  duration: 1200,
})
</script>
</body>
</html>
