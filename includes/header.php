<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Princewill" content="">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="wilsco">
    <meta name="keyword" content="wilsco.nl,wilsco,WILSCO,Wilsco.nl,Wilsco,oil tanks,oil tankers,storages,tankers,oil storage">
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@300;400&display=swap" rel="stylesheet">

    <meta name="Princewill" content="">
    <!-- <link rel="shortcut icon" type="image/png" href="img/genericS/logo/favicon-16x16.png"/> -->
    <link rel="shortcut icon" type="image/ico" href="img/genericS/logo/favicon.ico"/>
    <title><?php echo $title ?>  | IEVICON </title>
    
    <!-- fonts -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Exo+2:100,400" rel="stylesheet"> -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet"> -->
    <!-- https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css -->

	<!--cdn-->
    <!-- <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous"> -->
	<!-- <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"> -->
	<!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.css" integrity="sha256-gVCm5mRCmW9kVgsSjQ7/5TLtXqvfCoxhdsjE6O1QLm8=" crossorigin="anonymous" /> -->



    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">

    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

     <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/oas.css">
    <link rel="stylesheet" href="css/layout/header.css">
    <!-- <link rel="stylesheet" href="css/pages/home.css"> -->
    <!-- <link rel="stylesheet" href="css/pages/about.css"> -->
    <!-- <link rel="stylesheet" href="css/pages//storage.css"> -->
    <!-- <link rel="stylesheet" href="css/pages/healthsafety.css"> -->
    <!-- <link rel="stylesheet" href="css/pages/transportationLogistic.css"> -->
   
    <!-- <link rel="stylesheet" href="css/pages/quality.css"> -->
    <!-- <link rel="stylesheet" href="css/layouts/footer.css"> -->
    <!-- <link rel="stylesheet" href="../css/layouts/footer.css"> -->

<!--     <link rel="stylesheet" href="css/style.min.css"> -->
    <link rel="stylesheet" href="css/style.css">
  
    <!--<link rel="stylesheet" href="css/responsive.css">-->
    <!--<link rel="stylesheet" href="css/animation.css">-->
    <!--<link rel="stylesheet" href="css/font-awesome.min.css">-->
    <!--<link rel="stylesheet" href="css/animate.css">-->

    <style>
      .svg{
  max-width: 100%;
    width: 450px;  
    /* // min-height: 200px;
    //         width: 100%; */
    animation: fill 5s ease forwards 4.2s
}
.st0{font-size:20}

.svg path:nth-child(1){
   stroke-dasharray: 227.6px;
    stroke-dashoffset: 227.6px;
    animation: line-anim 4s ease forwards;
}
.svg path:nth-child(2){
   stroke-dasharray: 291.9px;
    stroke-dashoffset: 291.9px;
    animation: line-anim 4s ease forwards .2s;
}
.svg path:nth-child(3){
   stroke-dasharray: 230.24px;
    stroke-dashoffset: 230.24px;
    animation: line-anim 4s ease forwards .4s;
}
.svg path:nth-child(4){
   stroke-dasharray: 258.9px;
    stroke-dashoffset: 258.9px;
    animation: line-anim 4s ease forwards .6s;
}
.svg path:nth-child(5){
   stroke-dasharray: 179.6px;
    stroke-dashoffset: 179.6px;
    animation: line-anim 4s ease forwards .8s;
}

.svg path:nth-child(6){
   stroke-dasharray: 109.88px;
    stroke-dashoffset: 109.88px;
    animation: line-anim 4s ease forwards 1.0s;
}
.svg path:nth-child(7){
   stroke-dasharray: 224.166px;
    stroke-dashoffset: 224.166px;
    animation: line-anim 4s ease forwards 1.2s;
}
.svg path:nth-child(8){
   stroke-dasharray: 230.23px;
    stroke-dashoffset: 230.23px;
    animation: line-anim 4s ease forwards 1.4s;
}
.svg path:nth-child(9){
   stroke-dasharray: 311.93px;
    stroke-dashoffset: 311.93px;
    animation: line-anim 4s ease forwards 1.6s;
}
.svg path:nth-child(10){
   stroke-dasharray: 230.23px;
    stroke-dashoffset: 230.23px;
    animation: line-anim 4s ease forwards 1.8s ;
}

.svg path:nth-child(11){
   stroke-dasharray: 210.23px;
    stroke-dashoffset: 210.23px;
    animation: line-anim 4s ease forwards 2.0s;
}
.svg path:nth-child(12){
   stroke-dasharray: 109.88px;
    stroke-dashoffset: 109.88px;
    animation: line-anim 4s ease forwards 2.2s;
}
.svg path:nth-child(13){
   stroke-dasharray: 272.25px;
    stroke-dashoffset: 272.25px;
    animation: line-anim 4s ease forwards 2.4s;
}
.svg path:nth-child(14){
   stroke-dasharray: 324.07px;
    stroke-dashoffset: 324.07px;
    animation: line-anim 4s ease forwards 2.6s;
}
.svg path:nth-child(15){
   stroke-dasharray: 210.23px;
    stroke-dashoffset: 210.23px;
    animation: line-anim 4s ease forwards 2.8s;
}

.svg path:nth-child(16){
   stroke-dasharray: 284.23px;
    stroke-dashoffset: 284.23px;
     animation: line-anim 4s ease forwards 3.0s;
}
.svg path:nth-child(17){
   stroke-dasharray: 256.87px;
    stroke-dashoffset: 256.87px;
     animation: line-anim 4s ease forwards 3.2s;
}
.svg path:nth-child(18){
   stroke-dasharray: 225.29px;
    stroke-dashoffset: 225.29px;
     animation: line-anim 4s ease forwards 3.4s;
}
.svg path:nth-child(19){
   stroke-dasharray: 225.29px;
    stroke-dashoffset: 225.29px;
     animation: line-anim 4s ease forwards 3.6s;
}
.svg path:nth-child(20){
   stroke-dasharray: 230.24px;
    stroke-dashoffset: 230.24px;
     animation: line-anim 4s ease forwards 3.8s;
}
.svg path:nth-child(21){
   stroke-dasharray: 324.07px;
    stroke-dashoffset: 324.07px;
     animation: line-anim 4s ease forwards 4.1s;
}

@keyframes line-anim {
  to{
    stroke-dashoffset: 0;
  }
}
@keyframes fill {
  from {
    fill: transparent;
  }
  to{
    fill: white;
  }
}


    </style>

</head>
<body>


<section id="header">
    <!--============================ top bar =============================================-->
   
    <!--============================ nav bar =============================================-->
   <section id="navbar">
   <nav class="navbar navbar-expand-lg bg">
          <div class="container-fluid">
            <a class="navbar-brand" href=index.php>
              <img src="img/genericS/logo/Logo-01.png" alt="Logo" style="width:120px;">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
          
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                  <a class="nav-link" <?php if($page=='iv-graphics'){echo 'active';}?>" href="iv-graphics.php">iv graphics </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link <?php if($page=='iv-render'){echo 'active';}?>" href="iv-render.php">iv render</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link <?php if($page=='iv-motion'){echo 'active';}?>" href="iv-motion.php" tabindex="-1">iv motion</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link " href="#" tabindex="-1">iv trend</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link <?php if($page=='iv-exclusive'){echo 'active';}?>" href="iv-exclusive.php"tabindex="-1">iv exclusive</a>
                </li>
              </ul>
              <ul class="navbar-nav my-2 my-lg-0">
                <li class="nav-item">
                  <a class="nav-link" href="#">discover</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">support</a>
                </li>
              </ul>
            </div>
          </div>
       </nav>
    </section> 
</section>

