<section class="call-to-action <?php echo $class ?>"  >
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12">
				<div class="content">
					<h3 style="color:<?php echo $color ?>"><?php echo $text ?></h3>
					<a href="#" class="btn">Get in touch</a>
				</div>
			</div>
		</div>
	</div>
</section>