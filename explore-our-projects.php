
<?php
	$title = 'EXPLORE OUR PAST PROJECTS - IV RENDER';
	$page ="iv-render";
	include "includes/header.php";
?>

<section id="iv-explore-bg">
	<div class="container-fluid">
    <div class="row">
        <div class="col-md-4">
           <div class="contain">
              <img src="img/iv-render/icons/sub-EXPLORE-logo.png" alt="iv-render sub-logo">
           </div>
        </div>
        <div class="col-md-8">
          <div class="contain">
            <div class="content-wrap">
                <h2>EXPLORE OUR WORKS</h2>
            </div>
                   
           </div>
        </div>
    </div>

        <!-- <div class="container iv-container">
            <div class="row iv-row">
                    <div class="col-md-4 iv-sub">
                        <div class="sub-menu">
                            <div class="logo">
                                <img src="img/iv-render/icons/sub-EXPLORE-logo.png" alt="iv-render sub-logo">
                            </div>
                        </div>
                </div>
                <div class="col-md-8 iv-content">
                        <div class="main-content">
                            <div class="content-wrap">
                                <h2>EXPLORE OUR WORKS</h2>
                            </div>
                        </div>
                </div>
            </div>
        </div> -->
	</div>
</section>
<!-- ========================================================================================================= -->
                 <!-- CATEGORY -->
<!-- ========================================================================================================= -->
<section class="iv-explore box-8-4">
   <div class="container-fluid">
	<div class="row">
     <div class="col-md-8 box-8">
	    <div class="image-container">
            <img src="img/iv-render/slides/1.jpg" class="img-fuild" alt="slide 1">
        </div>
      </div>
	  <div class="col-md-4 box-4">
	    <div class="content">
                <h4>Residential interior visualization</h4>
                <p>Architech Lanre</p>
           	   
		</div>
	  </div>
	
	</div>
   </div>
</section>

<!-- ========================================================================================================= -->
                 <!-- 2nd -->
<!-- ========================================================================================================= -->

<section class="iv-explore box-8-4">
   <div class="container-fluid">
	<div class="row">
    <div class="col-md-4 box-4">
	    <div class="content">
                <h4> exterior visualization for corforate building</h4>
                <p>Joe & James Architecture</p>
           	   
		</div>
	  </div>
     <div class="col-md-8 box-8">
	    <div class="image-container">
            <img src="img/iv-render/slides/13.jpg" class="img-fuild" alt="slide 1">
        </div>
      </div>
	
	
	</div>
   </div>
</section>

<!-- ========================================================================================================= -->
                 <!-- 3rd-->
<!-- ========================================================================================================= -->
<section class="iv-explore box-8-4">
   <div class="container-fluid">
	<div class="row">
     <div class="col-md-8 box-8">
	    <div class="image-container">
            <img src="img/iv-render/slides/3.jpg" class="img-fuild" alt="slide 1">
        </div>
      </div>
	  <div class="col-md-4 box-4">
	    <div class="content">
                <h4>3D EXTERIOR RENDERING</h4>
                <p>Gritz Consultium</p>
           	   
		</div>
	  </div>
	
	</div>
   </div>
</section>

<!-- ========================================================================================================= -->
                 <!-- FOUR -->
<!-- ========================================================================================================= -->

<section class="iv-explore box-8-4">
   <div class="container-fluid">
	<div class="row">
    <div class="col-md-4 box-4">
	    <div class="content">
                <h4> interior rendering</h4>
                <p>Architect Tunde Jekayinfa</p>
           	   
		</div>
	  </div>
     <div class="col-md-8 box-8">
	    <div class="image-container">
            <img src="img/iv-render/slides/10.jpg" class="img-fuild" alt="slide 1">
        </div>
      </div>
	
	
	</div>
   </div>
</section>
<!-- ========================================================================================================= -->
                 <!-- five-->
<!-- ========================================================================================================= -->
<section class="iv-explore box-8-4">
   <div class="container-fluid">
	<div class="row">
     <div class="col-md-8 box-8">
	    <div class="image-container">
            <img src="img/iv-render/slides/7.jpg" class="img-fuild" alt="slide 1">
        </div>
      </div>
	  <div class="col-md-4 box-4">
	    <div class="content">
                <h4>Architectural animation</h4>
                <p>G & D Solutions</p>
           	   
		</div>
	  </div>
	
	</div>
   </div>
</section>
<!-- ========================================================================================================= -->
                 <!-- six -->
<!-- ========================================================================================================= -->

<section class="iv-explore box-8-4">
   <div class="container-fluid">
	<div class="row">
    <div class="col-md-4 box-4">
	    <div class="content">
                <h4> interior rendering</h4>
                <p>Arch Ikenne & Co.</p>
           	   
		</div>
	  </div>
     <div class="col-md-8 box-8">
	    <div class="image-container">
            <img src="img/iv-render/slides/2.jpg" class="img-fuild" alt="slide 1">
        </div>
      </div>
	
	
	</div>
   </div>
</section>
<!-- ========================================================================================================= -->
                 <!-- 7-->
<!-- ========================================================================================================= -->
<section class="iv-explore box-8-4">
   <div class="container-fluid">
	<div class="row">
     <div class="col-md-8 box-8">
	    <div class="image-container">
            <img src="img/iv-render/slides/12.jpg" class="img-fuild" alt="slide 1">
        </div>
      </div>
	  <div class="col-md-4 box-4">
	    <div class="content">
                <h4>interior rendering</h4>
                <p>Arch Ikenne & Co.</p>
           	   
		</div>
	  </div>
	
	</div>
   </div>
</section>

<!-- ========================================================================================================= -->
                 <!-- 8 -->
<!-- ========================================================================================================= -->

<section class="iv-explore box-8-4">
   <div class="container-fluid">
	<div class="row">
    <div class="col-md-4 box-4">
	    <div class="content">
                <h4> proposed interior for ievicon</h4>
                <p>G & D Solutions</p>
           	   
		</div>
	  </div>
     <div class="col-md-8 box-8">
	    <div class="image-container">
            <img src="img/iv-render/slides/8.jpg" class="img-fuild" alt="slide 1">
        </div>
      </div>
	
	
	</div>
   </div>
</section>
<!-- ---------------------------------------------------------- -->
<section class="load-more">
   <div class="container">
	<div class="row">
    <div class="col-md-12">
        <h4>Load More <i class="fa fa-angle-double-down"></i></h4>
    </div>
	</div>
   </div>
</section>

<!-- ========================================================================================================= -->
                 <!-- 9-->
<!-- ========================================================================================================= -->
<!-- <section class="iv-explore box-8-4">
   <div class="container-fluid">
	<div class="row">
     <div class="col-md-8 box-8">
	    <div class="image-container">
            <img src="img/iv-render/slides/7.jpg" class="img-fuild" alt="slide 1">
        </div>
      </div>
	  <div class="col-md-4 box-4">
	    <div class="content">
                <h4>Architectural animation</h4>
                <p>G & D Solutions</p>
           	   
		</div>
	  </div>
	
	</div>
   </div>
</section> -->
<!--=========================footer =============================================-->
 <!-- ----------footer---------------------------------- -->
 <?php
	$page ="iv-render";
    include "includes/footer.php";
?>
<!-- ---------------end footer ---------------------------- -->
