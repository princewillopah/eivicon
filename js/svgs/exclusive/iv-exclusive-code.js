bodymovin.loadAnimation({
    container: document.getElementById('exclusive_hero'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: 'jsons/exclusive/iv-exclusive-data.json'
  })

var pivotcoins = bodymovin.loadAnimation({
    container: document.getElementById('pivotcoins'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: 'jsons/exclusive/pivotcoins/pivotcoins_data.json'
  })

  var hoyer = bodymovin.loadAnimation({
    container: document.getElementById('hoyer'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: 'jsons/exclusive/hoyer/hoyer_data.json'
  })
  var ekstralayn = bodymovin.loadAnimation({
    container: document.getElementById('ekstralayn'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: 'jsons/exclusive/ekstralayn/ekstralayn_data.json'
  })

  var snd = bodymovin.loadAnimation({
    container: document.getElementById('snd'),
     renderer: 'svg',
    loop:true,
    autoplay:true,
    path:'jsons/exclusive/snd/snd_data.json'
  })
  var deeperlife = bodymovin.loadAnimation({
    container: document.getElementById('deeperlife'),
     renderer: 'svg',
    loop:true,
    autoplay:true,
    path:'jsons/exclusive/deeperlife/deeperlife_data.json'
  })

  var benco = bodymovin.loadAnimation({
    container: document.getElementById('benco'),
     renderer: 'svg',
    loop:true,
    autoplay:true,
    path:'jsons/exclusive/benco/benco_data.json'
  })
 
   bodymovin.loadAnimation({
    container: document.getElementById('hero_svg'),
     renderer: 'svg',
    loop:true,
    autoplay:true,
    path:'jsons/exclusive/hero/hero_data.json'
  })