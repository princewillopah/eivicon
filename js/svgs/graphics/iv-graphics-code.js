bodymovin.loadAnimation({
    container: document.getElementById('iv-graphics'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: 'jsons/graphics/graphics_data.json'
  })

bodymovin.loadAnimation({
    container: document.getElementById('brand-for-fashion-svg'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: 'jsons/graphics/brand-for-fashion-svg-data.json'
  })

bodymovin.loadAnimation({
    container: document.getElementById('first-section-svg'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: 'jsons/graphics/works/first-section/data.json'
  })
bodymovin.loadAnimation({
    container: document.getElementById('fourth-section-a-svg'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: 'jsons/graphics/works/fourt-section/fourth-section-a-data.json'
  })
bodymovin.loadAnimation({
    container: document.getElementById('fourth-section-b-svg'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: 'jsons/graphics/works/fourt-section/fourth-section-b-data.json'
  })
bodymovin.loadAnimation({
    container: document.getElementById('fifth-svg'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: 'jsons/graphics/works/fifth-section/fifth-data.json'
  })
bodymovin.loadAnimation({
    container: document.getElementById('sixth-svg'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: 'jsons/graphics/works/sixth-section/sixth-section-data.json'
  })
bodymovin.loadAnimation({
    container: document.getElementById('eight-svg'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: 'jsons/graphics/works/eight-section/eight-data.json'
  })