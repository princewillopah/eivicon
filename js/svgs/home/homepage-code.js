var animation = bodymovin.loadAnimation({
    container: document.getElementById('hero'),
     renderer: 'svg',
    loop:true,
    autoplay:true,
    path:'jsons/home/hero_data.json'
  })
  var animation2 = bodymovin.loadAnimation({
    container: document.getElementById('graph'),
     renderer: 'svg',
    loop:true,
    autoplay:true,
    path:'jsons/home/graph_data.json'
  })
  var slide1 = bodymovin.loadAnimation({
    container: document.getElementById('slide1'),
     renderer: 'svg',
    loop:true,
    autoplay:true,
    path:'jsons/home/slide1/slide1_data.json'
  })
  var slide2 = bodymovin.loadAnimation({
    container: document.getElementById('slide2'),
     renderer: 'svg',
    loop:true,
    autoplay:true,
    path:'jsons/home/slide2/slide2_data.json'
  })
  var slide3 = bodymovin.loadAnimation({
    container: document.getElementById('slide3'),
     renderer: 'svg',
    loop:true,
    autoplay:true,
    path:'jsons/home/slide3/slide3_data.json'
  })
  var slide4 = bodymovin.loadAnimation({
    container: document.getElementById('slide4'),
     renderer: 'svg',
    loop:true,
    autoplay:true,
    path:'jsons/home/slide4/slide4_data.json'
  })