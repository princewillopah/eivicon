<?php
	$title = 'Home';
	$page ="home";
	include "includes/header.php";
?>

 <section id="hero-page">
  <div class="inner-hero">
       <div class="container-fluid">
         <div class="row">
           <div class="col-md-7 ">
             <div class="hero-left">
               <div class="content">
                 <p>A ORB WHERE</p>
                

<svg class="svg" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 598.84 136.05" style="enable-background:new 0 0 598.84 136.05;" xml:space="preserve" fill="none">
<style type="text/css">
	.st0{stroke:#FFFFFF;stroke-width:2;}
</style>
<g>
	<path class="st0" d="M31.94,52.5c-6.55,0-12.18-2.24-16.88-6.71C10.35,41.32,8,35.8,8,29.22c0-6.57,2.06-12.1,6.17-16.57
		c4.12-4.47,9.39-6.71,15.81-6.71c5.04,0,10.14,0.84,15.31,2.52l-0.82,2.52c-5.04-1.51-9.87-2.27-14.49-2.27
		c-5.5,0-10,1.97-13.48,5.92c-3.49,3.95-5.23,8.78-5.23,14.49c0,5.71,2.03,10.57,6.08,14.58c4.05,4.01,8.91,6.02,14.58,6.02
		c4.96,0,9.24-0.76,12.85-2.27l0.95,2.14c-1.6,0.92-3.69,1.64-6.27,2.14S34.37,52.5,31.94,52.5z"/>
	<path class="st0" d="M57.58,51.74V6.89c3.99-0.29,7.33-0.44,10.02-0.44c4.28,0,7.81,1.1,10.58,3.31c2.77,2.2,4.16,5.32,4.16,9.36
		c0,2.73-0.8,5.27-2.39,7.62c-1.6,2.35-3.38,4.22-5.35,5.61c1.55,2.14,3.47,4.69,5.76,7.65c2.29,2.96,3.77,4.82,4.44,5.58
		c2.56,2.73,5.46,4.28,8.69,4.66l-0.06,2.02c-2.56-0.04-4.64-0.43-6.24-1.17c-1.6-0.73-3.12-1.86-4.57-3.37
		c-1.45-1.51-5.05-6.22-10.8-14.11c-4.12,0-7.85-0.13-11.21-0.38v18.52H57.58z M69.99,31.08c1.85,0,3.82-1.22,5.92-3.65
		c2.1-2.44,3.15-5.26,3.15-8.47s-1.08-5.65-3.24-7.31c-2.16-1.66-5.03-2.49-8.6-2.49c-1.89,0-4.1,0.08-6.62,0.25v21.36
		C63.96,30.98,67.09,31.08,69.99,31.08z"/>
	<path class="st0" d="M99.03,51.74V6.7h20.92v2.65h-17.89v16.57h16.19v2.58h-16.19v20.6h18.58v2.65H99.03z"/>
	<path class="st0" d="M135.13,36.12l-6.49,15.62h-3.21l18.96-45.36h1.76l19.03,45.36h-3.4l-6.49-15.62H135.13z M145.08,11.42
		c-0.04,0.04-0.13,0.29-0.25,0.76c-0.59,1.81-1.16,3.34-1.7,4.6l-6.93,16.76h18.02l-6.93-16.69
		C145.82,13.65,145.08,11.84,145.08,11.42z"/>
	<path class="st0" d="M178.03,9.35h-15.25V6.7h33.52v2.65h-15.25v42.4h-3.02V9.35z"/>
	<path class="st0" d="M206,51.74V6.7h3.02v45.04H206z"/>
	<path class="st0" d="M240.65,42.61L255.77,6.7h3.21l-19.4,45.42h-1.76L218.35,6.7h3.4l15.12,35.78c1.05,2.56,1.7,4.33,1.95,5.29
		C239.16,46.43,239.77,44.71,240.65,42.61z"/>
	<path class="st0" d="M268.31,51.74V6.7h20.92v2.65h-17.89v16.57h16.19v2.58h-16.19v20.6h18.59v2.65H268.31z"/>
	<path class="st0" d="M337.67,6.26c6.93,0,12.73,2.15,17.39,6.46c4.66,4.31,6.99,10.15,6.99,17.55c0,3.74-0.6,7.05-1.79,9.92
		c-1.2,2.88-2.85,5.18-4.95,6.9c-4.33,3.49-9.72,5.23-16.19,5.23c-4.24,0-9.05-0.23-14.43-0.69V6.7
		C328.98,6.41,333.3,6.26,337.67,6.26z M339.12,49.54c5.92,0,10.68-1.61,14.27-4.82s5.39-8.05,5.39-14.52
		c0-6.47-2.03-11.61-6.08-15.44c-4.05-3.82-9.06-5.73-15.03-5.73c-3.82,0-7.14,0.1-9.95,0.31v39.69
		C330.45,49.37,334.25,49.54,339.12,49.54z"/>
	<path class="st0" d="M376.04,51.74V6.7h20.92v2.65h-17.89v16.57h16.19v2.58h-16.19v20.6h18.58v2.65H376.04z"/>
	<path class="st0" d="M429.21,10.67c-3.07-1.3-6.11-1.95-9.13-1.95c-6.13,0-9.2,2.27-9.2,6.8c0,1.97,0.69,3.81,2.08,5.51
		c1.39,1.7,3.07,3.21,5.04,4.54c1.97,1.32,3.95,2.68,5.92,4.06c1.97,1.39,3.65,3.05,5.04,4.98c1.39,1.93,2.08,4.03,2.08,6.3
		c0,3.82-1.28,6.71-3.84,8.66c-2.56,1.95-6.1,2.93-10.62,2.93c-4.52,0-8.37-1.15-11.56-3.46l1.51-2.27
		c3.02,1.97,6.34,2.96,9.95,2.96s6.38-0.72,8.32-2.17c1.93-1.45,2.9-3.54,2.9-6.27c0-1.89-0.69-3.69-2.08-5.39
		c-1.39-1.7-3.07-3.23-5.04-4.6c-1.97-1.36-3.95-2.76-5.92-4.19c-1.98-1.43-3.65-3.13-5.04-5.1c-1.39-1.97-2.08-4.1-2.08-6.36
		c0-2.98,1.1-5.34,3.31-7.09c2.2-1.74,5.19-2.61,8.95-2.61s7.25,0.76,10.49,2.27L429.21,10.67z"/>
	<path class="st0" d="M444.2,51.74V6.7h3.02v45.04H444.2z"/>
	<path class="st0" d="M469.72,14.64c-3.49,3.95-5.23,8.79-5.23,14.52s1.97,10.59,5.92,14.58c3.95,3.99,8.76,5.98,14.43,5.98
		c4.87,0,8.92-0.67,12.16-2.02V29.19h3.02V49.1c-3.65,2.27-8.77,3.4-15.34,3.4c-6.57,0-12.13-2.26-16.66-6.77
		c-4.54-4.51-6.8-10.04-6.8-16.57c0-6.53,2.06-12.03,6.17-16.51c4.12-4.47,9.39-6.71,15.81-6.71c5.04,0,10.14,0.84,15.31,2.52
		l-0.82,2.52c-5.04-1.51-9.87-2.27-14.49-2.27C477.7,8.72,473.2,10.69,469.72,14.64z"/>
	<path class="st0" d="M518.54,13.88l0.13,5.35v32.51h-3.02V6.32h1.39l29.67,34.21c2.02,2.39,3.57,4.31,4.66,5.73
		c-0.17-2.02-0.25-4.33-0.25-6.93V6.7h3.02v45.42h-1.39l-30.24-34.84c-2.1-2.39-3.44-3.99-4.03-4.79L518.54,13.88z"/>
	<path class="st0" d="M590.23,10.67c-3.07-1.3-6.11-1.95-9.13-1.95c-6.13,0-9.2,2.27-9.2,6.8c0,1.97,0.69,3.81,2.08,5.51
		c1.39,1.7,3.07,3.21,5.04,4.54c1.97,1.32,3.95,2.68,5.92,4.06c1.97,1.39,3.65,3.05,5.04,4.98c1.39,1.93,2.08,4.03,2.08,6.3
		c0,3.82-1.28,6.71-3.84,8.66c-2.56,1.95-6.1,2.93-10.62,2.93c-4.52,0-8.37-1.15-11.56-3.46l1.51-2.27
		c3.02,1.97,6.34,2.96,9.95,2.96s6.38-0.72,8.32-2.17c1.93-1.45,2.9-3.54,2.9-6.27c0-1.89-0.69-3.69-2.08-5.39
		c-1.39-1.7-3.07-3.23-5.04-4.6c-1.97-1.36-3.95-2.76-5.92-4.19c-1.98-1.43-3.65-3.13-5.04-5.1c-1.39-1.97-2.08-4.1-2.08-6.36
		c0-2.98,1.1-5.34,3.31-7.09c2.2-1.74,5.19-2.61,8.95-2.61s7.25,0.76,10.49,2.27L590.23,10.67z"/>
	<path class="st0" d="M44.6,127.34v-22.55H13.67v22.55h-3.02V82.3h3.02v19.78H44.6V82.3h3.02v45.04H44.6z"/>
	<path class="st0" d="M66.65,111.72l-6.49,15.62h-3.21l18.96-45.36h1.76l19.03,45.36h-3.4l-6.49-15.62H66.65z M76.6,87.02
		c-0.04,0.04-0.13,0.29-0.25,0.76c-0.59,1.81-1.16,3.34-1.7,4.6l-6.93,16.76h18.02l-6.93-16.69C77.34,89.25,76.6,87.44,76.6,87.02z"
		/>
	<path class="st0" d="M106.02,127.34V82.55c3.49-0.34,6.87-0.5,10.14-0.5c4.41,0,7.95,1.03,10.62,3.09c2.67,2.06,4,5.16,4,9.29
		c0,4.14-1.82,7.54-5.45,10.21c-3.63,2.67-7.97,4-13.01,4c-0.21,0-0.44,0-0.69,0v-2.08c0.17,0,0.31,0,0.44,0
		c4.16,0,7.77-1.09,10.84-3.28c3.06-2.18,4.6-5.03,4.6-8.54c0-3.51-1.07-6.05-3.21-7.62c-2.14-1.57-5.02-2.36-8.63-2.36
		c-1.89,0-4.1,0.08-6.62,0.25v42.34H106.02z"/>
	<path class="st0" d="M142.63,127.34V82.55c3.49-0.34,6.87-0.5,10.14-0.5c4.41,0,7.95,1.03,10.62,3.09c2.67,2.06,4,5.16,4,9.29
		c0,4.14-1.82,7.54-5.45,10.21c-3.63,2.67-7.97,4-13.01,4c-0.21,0-0.44,0-0.69,0v-2.08c0.17,0,0.31,0,0.44,0
		c4.16,0,7.77-1.09,10.84-3.28c3.06-2.18,4.6-5.03,4.6-8.54c0-3.51-1.07-6.05-3.21-7.62c-2.14-1.57-5.02-2.36-8.63-2.36
		c-1.89,0-4.1,0.08-6.62,0.25v42.34H142.63z"/>
	<path class="st0" d="M179.23,127.34V82.3h20.92v2.65h-17.89v16.57h16.19v2.58h-16.19v20.6h18.58v2.65H179.23z"/>
	<path class="st0" d="M215.83,89.48l0.13,5.35v32.51h-3.02V81.92h1.39l29.67,34.21c2.02,2.39,3.57,4.31,4.66,5.73
		c-0.17-2.02-0.25-4.32-0.25-6.93V82.3h3.02v45.42h-1.39L219.8,92.88c-2.1-2.39-3.44-3.99-4.03-4.79L215.83,89.48z"/>
</g>
</svg>


                <a href="#">Read More</a>

               </div>
             </div>
           </div>
           <div class="col-md-5">
            <div id="hero" class="img-container">
            </div>
           </div>
         </div>
       </div>
  </div>
</section> 

<!-- ==================================================================================================================== -->
<section class="slideshows" >
      <div class="container">
          <div class="row">           
              <div class="col-md-6">
                <div class="js-carousel-1 owl-carousel owl-theme">
                   <div class="item">
                     <div class="slideshowse-image">
                       <div id="slide1"></div>
                     </div>
                    </div>
                    <div class="item">
                     <div class="slideshowse-image">
                       <div id="slide2"></div>
                     </div>
                    </div>
                    <div class="item">
                     <div class="slideshowse-image">
                       <div id="slide3"></div>
                     </div>
                    </div>
                    <div class="item">
                     <div class="slideshowse-image">
                       <div id="slide4"></div>
                     </div>
                    </div>
                </div>
              </div>
                            
                  <div class="col-md-6 slideshows-text-col">
                    <div class="js-carousel-2 owl-carousel owl-theme second">
                      <div class="item ">
                            <div class="slideshows-text">
                              <h2>Exceptional 3d Rendering</h2>
                              <p>Able to see your proposed design solutions and the elaborate architecture 
                                details, your clients can approve your work quickly. That makes photorealistic 
                                3D rendering an unparalleled tool used in client presentations such as 
                                Real Estates, Interior Designers, Corporate Offices, Architects & Designers.
                              </p>
                              <a href="#" class="btn slideshows-btn">READ MORE</a>
                          </div>
                        </div>

                      <div class="item">
                            <div class="slideshows-text">
                              <h2>Motion Pictures</h2>
                              <p>Motion pictures ­ a new way in mass communication. 
                                Communicating your ideas with motion pictures that best execute your vision
                                to your audience.
                              </p>
                              <a href="#" class="btn slideshows-btn">READ MORE</a>
                          </div>
                        </div>


                        <div class="item">
                        <div class="slideshows-text">
                          <h2>WEBSITE DEVELOPMENT</h2>
                          <p>Modern websites with advancement and unused inovations that match all your business solutions.
                          </p>
                          <a href="#" class="btn slideshows-btn">READ MORE</a>
                           </div>
                        </div>

                        <div class="item">
                          <div class="slideshows-text">
                            <h2>Communicating Brands</h2>
                            <p>Keeping your brand top of mind is also imperative to influence a consumer’s
                                buying decision. Many consumers today lack trust in brands, so making your 
                                brand authentic is a challenge every marketer faces.
                            </p>
                            <a href="#" class="btn slideshows-btn">READ MORE</a>
                          </div>
                        </div>
                      
                    </div>
                  </div>         
          </div>
      </div>
  </section>







<!-- <section class="slideshows">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="slideshowse-image">
           <img src="img/home/brand-identity-png.png" class="img-fluid" alt="">
        </div>
      </div>
      <div class="col-md-6 slideshows-text-col">
        <div class="slideshows-text">
            <h2>invest in branding</h2>
            <h3>starting with yourself</h3>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. 
              Et incidunt magni tempore doloremque alias, 
              officia quod beatae nisi, nemo labore, in iure! 
              Illum voluptatum assumenda hic totam alias ut non.
            </p>
            <a href="#" class="btn slideshows-btn">READ MORE</a>
        </div>
      </div>
    </div>
  </div>
</section> -->

<!-- ==================================================================================================================== -->
<section class="latest-projects">
  <section class="latest-projects-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="latest-projects-title-text">
            <h5>explore our latest projects</h5>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="latest-projects-image">
    <div class="container-fluid">
		<div class="row">
			<div class="col-md-7">
				<div class="latest-projects-image-img">
					<img src="img/home/our-project-section/IV Graphics.png" class="img-fluid" alt="">
				</div>
			</div>
			<div class="col-md-5">
				<div class="latest-projects-image-content right" data-aos="anim-right">
          <span data-aos="anim-right1"></span>
          <span data-aos="anim-right2"></span>
          <span data-aos="anim-right3"></span>
          <span></span>
            <h2>LISIBELLE MULTICONCEPTS</h2>
            <p> <i class="fa fa-angle-right"></i> CORPORATE BRANDING
            </p>
				</div>
			</div>
		</div>
	</div>
  </section>

  <section class="latest-projects-image">
    <div class="container-fluid">
		<div class="row">
			<div class="col-md-5">
				<div class="latest-projects-image-content left" >
          <span data-aos="anim-left1"></span>
          <span></span>
          <span data-aos="anim-left3"></span>
          <span data-aos="anim-left4"></span>
          <h2>LISE</h2>
          <p> <i class="fa fa-angle-right"></i> 3D RENDERING FOR EVENT PLANER
          </p>
           
				</div>
			</div>
			
			<div class="col-md-7">
				<div class="latest-projects-image-img">
					<img src="img/home/our-project-section/IV Render.jpg" class="img-fluid" alt="">
				</div>
			</div>
		</div>
	</div>
  </section>

  <section class="latest-projects-image last">
    <div class="container-fluid">
		<div class="row">
			<div class="col-md-7">
				<div class="latest-projects-image-img">
          <video id='my-video' class='video-css' controls preload='auto' muted width='100%' height='100%'
                           poster='MY_VIDEO_POSTER.jpg' data-setup='{}' controls autoplay>
             <source src='img/home/our-project-section/IV Motion.mp4' type='video/mp4'>
           </video>
				</div>
			</div>
			<div class="col-md-5">
				<div class="latest-projects-image-content right last" data-aos="anim-right">
          <span data-aos="anim-right1"></span>
          <span data-aos="anim-right2"></span>
          <span data-aos="anim-right3"></span>
          <span></span>
            <h2>PIVOTCOINS</h2>
            <p> <i class="fa fa-angle-right"></i> CORPORATE EXPLAINER VIDEO
            </p>
				</div>
			</div>
		</div>
	</div>
  </section>

  <!-- <section class="latest-projects-footer-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
            <h5> <i class="fa fa-angle-right"></i> SEE ALL OUR PROJECTS</h5>
        </div>
      </div>
    </div>
  </section> -->

</section>

<!-- --------------------------------------------------------------------------------------------------- -->
<!-- ------------------------------------------------------------------------------------------------ -->

<section class="we-are-commited">
   <div class="container">
	   <div class="row">
		   <div class="col-md-5">
			   <div class="we-are-commited-content">
				   <h5>We are commited</h5>
				   <p>Our customer service index officer keeps extensive track records
              of our customers on how we have improved on providing the best
               customer values to increase their business needs</p>
			   </div>
		   </div>
		   <div class="col-md-7">
			   <div class="we-are-commited-img">
           <div class="graph" id="graph"></div>
			   </div>
		   </div>
	   </div>
   </div>
</section>

<!-- --------------------------------------------------------------------------------------------------- -->
<!-- ------------------------------------------------------------------------------------------------ -->

<section class="our-partners">
   <div class="container">
	   <div class="row">
		   <div class="col-md-12">
			   <div class="our-partners-content">
				   <h5>CREATING IMAGINATIVE DEVELOPMENT FOR BRANDS</h5>
				   <p>We have partnered with several brands to create innovative successes that increase their business values</p>
			   </div>
		   </div>
		   <div class="col-md-12">
			   <div class="we-are-commited-img">
              <div class="we-are-commited-img-slide">
                  <div class="our-partners-owl-carousel owl-carousel owl-theme">
                      <div class="item" >
                              <div class="name-pic">
                                  <div class="pics">
                                      <img src="img/home/our-partners/Deeperlife.png" alt="" class="img-fluid">
                                  </div>
                              </div>
                      </div>
                      <div class="item" >
                              <div class="name-pic">
                                  <div class="pics">
                                      <img src="img/home/our-partners/George Dickson.png" alt="" class="img-fluid">
                                  </div>
                              </div>
                      </div>
                      <div class="item" >
                              <div class="name-pic">
                                  <div class="pics">
                                      <img src="img/home/our-partners/Lisibelle.png" alt="" class="img-fluid">
                                  </div>
                              </div>
                      </div>
                      <div class="item" >
                              <div class="name-pic">
                                  <div class="pics">
                                      <img src="img/home/our-partners/NTV.png" alt="" class="img-fluid">
                                  </div>
                              </div>
                      </div>
                      <div class="item" >
                              <div class="name-pic">
                                  <div class="pics">
                                      <img src="img/home/our-partners/Purpleboxx.png" alt="" class="img-fluid" >
                                  </div>
                              </div>
                      </div>
                      <div class="item" >
                              <div class="name-pic">
                                  <div class="pics">
                                      <img src="img/home/our-partners/Syntexa.png" alt="" class="img-fluid">
                                  </div>
                              </div>
                      </div>
                      <div class="item" >
                              <div class="name-pic">
                                  <div class="pics">
                                      <img src="img/home/our-partners/Peal House.png" alt="" class="img-fluid">
                                  </div>
                              </div>
                      </div>
                      <div class="item" >
                              <div class="name-pic">
                                  <div class="pics">
                                      <img src="img/home/our-partners/Peela.png" alt="" class="img-fluid">
                                  </div>
                              </div>
                      </div>

                  </div>
              </div>
                      
			   </div>
		   </div>
	   </div>
   </div>
</section>




<!--=========================footer =============================================-->
 <!-- ----------footer---------------------------------- -->
 <?php
	$page ="home";
	include "includes/footer.php";
?>
<!-- ---------------end footer ---------------------------- -->
