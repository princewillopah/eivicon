
<?php
	$title = 'IV GRAPHICS';
	$page ="iv-graphics";
	include "includes/header.php";
?>



<section class="iv-graphics-work first-section">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-8">
        <div class="content">
            <img src="img/iv-graphics/works/SakeNatural-Large-Screen.png" class="img-fluid" alt="">
        </div>
      </div>
      <div class="col-md-4">
      	           <div class="row">

          <div class="col-md-12 a">
            <div class="content">
                 <h3>work</h3>
            </div>
          </div>

          <div class="col-md-12 b">
            <div class="content">
               <div id="first-section-svg"></div>
            </div>
          </div>

        </div>
      </div>
      
    </div>
  </div>
</section>
<section class="iv-graphics-work second-section">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-4">
        <div class="row">
          <div class="col-md-12 a">
            <div class="content">
                 <img src="img/iv-graphics/works/InsightPlus-Large-Screen.png" class="img-fluid" alt="">
            </div>
          </div>
          <div class="col-md-12 b">
            <div class="content">
               <img src="img/iv-graphics/works/Fabulous-Large-Screen.png" class="img-fluid" alt="">
              
            </div>
            </div>
        </div>
      </div>
      <div class="col-md-8">
        <div class="content">
                       <img src="img/iv-graphics/works/Syntexa-Large-Screen.png" class="img-fluid" alt="">
        </div>
      </div>
      
    </div>
  </div>
</section>
<section class="iv-graphics-work third-section">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-4 a">
        <div class="content">
                   <img src="img/iv-graphics/works/SOAN1-Large-Screen.png" class="img-fluid" alt="">
        </div>
      </div>
      <div class="col-md-4 b">
        <div class="content">
                   <img src="img/iv-graphics/works/SOAN2-Large-Screen.png" class="img-fluid" alt="">
        </div>
      </div>
      <div class="col-md-4 c">
        <div class="content">
                   <img src="img/iv-graphics/works/SOAN3-Large-Screen.png" class="img-fluid" alt="">
        </div>
      </div>
      
      
    </div>
  </div>
</section>

<section class="iv-graphics-work fourth-section">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-4">
        <div class="content">
                 <div id="fourth-section-a-svg"></div>
        </div>
      </div>
      <div class="col-md-8">
        <div class="content">
                    <div id="fourth-section-b-svg"></div>
        </div>
      </div>
      
    </div>
  </div>
</section>
<section class="iv-graphics-work fifth-section">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-4 a">
        <div class="content">
                   <img src="img/iv-graphics/works/CIF-Large-Screen.jpg" class="img-fluid" alt="">
        </div>
      </div>
      <div class="col-md-4 b">
        <div class="content">
                   <img src="img/iv-graphics/works/DD-Largel-Screen.png" class="img-fluid" alt="">
        </div>
      </div>
      <div class="col-md-4 c">
        <div class="content">
           <div id="fifth-svg"></div>
        </div>
      </div>
      
      
    </div>
  </div>
</section>

<section class="iv-graphics-work sixth-section">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-8">
        <div class="content">
             <img src="img/iv-graphics/works/BeautySalon-Large-Screen.png" class="img-fluid" alt="">
        </div>
      </div>
      <div class="col-md-4">
        <div class="content">
              <div id="sixth-svg"></div>
        </div>
      </div>
      
    </div>
  </div>
</section>
<section class="iv-graphics-work seventh-section">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6 a">
        <div class="content">
             <img src="img/iv-graphics/works/Jones&Vine-Large-Screen.png" class="img-fluid" alt="">
        </div>
      </div>
      <div class="col-md-6 b">
        <div class="content">
               <img src="img/iv-graphics/works/Akohi-Large-Screen.png" class="img-fluid" alt="">
        </div>
      </div>
      
    </div>
  </div>
</section>
<section class="iv-graphics-work eight-section">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-8">
        <div class="content">
             <div id="eight-svg"></div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="content">
               <img src="img/iv-graphics/works/DC-Large-Screen.png" class="img-fluid" alt="">
        </div>
      </div>
      
    </div>
  </div>
</section>


 <?php
    $class ="iv-graphics";
    $text="Building Your Brand and Increase Sales is our Pride";
    include "includes/call-to-action.php";
 ?> 

<!--=========================footer =============================================-->
 <!-- ----------footer---------------------------------- -->
 <?php
	$page ="iv-graphics";
    include "includes/footer.php";
?>
<!-- ---------------end footer ---------------------------- -->
